package com.example.pavel;

import com.example.pavel.Person;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Service
public class MyService {
    List<Person> listPersons = new ArrayList<>();

    @ResponseBody
    public Person simpleMethod(Person person) {
        if (listPersons.add(person)) {
            int allSalary = (int) (person.getSalary() * 1.1 * 12);
            person.setAllSalary(allSalary);
            return person;
        }
        return null;
    }

}
