package com.example.pavel;

import com.example.pavel.Person;
import com.example.pavel.MyService;
import org.springframework.web.bind.annotation.*;

@RestController
public class MyController {
    private final MyService service;
    public MyController(MyService service) {
        this.service = service;

    }


    @PostMapping("/Person")
    public Person addPerson(@RequestBody Person person) {
        Person result = service.simpleMethod(person);
        return result;
    }
}
