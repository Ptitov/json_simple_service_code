package com.example.pavel;

public class Person {
    String name;
    String surName;
    int salary;

    int allSalary;

    public Person() {
    }

    public Person(String name, String surName, int salary) {
        this.name = name;
        this.surName = surName;
        this.salary = salary;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurName() {
        return this.surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public int getSalary() {
        return this.salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getAllSalary() {
        return this.allSalary;
    }

    public void setAllSalary(int allSalary) {
        this.allSalary = allSalary;
    }

}
